import bcolors
from executor import Executor
import sys, json, asyncio, threading

print(f'>>> {bcolors.HEADER}CF Guild Bot :: Initializing...{bcolors.ENDC}')

if (sys.argv.__len__() == 1):
    print(f'>>> {bcolors.FAIL}Secrets file not found.{bcolors.ENDC}')
    sys.exit()
else:
    try:
        with open(sys.argv[-1], 'r') as jsonFile:
            secret = json.load(jsonFile)
    except Exception as e:
        print(f'>>> {bcolors.FAIL}Unable to parse Secrets file.{bcolors.ENDC}\n\n{e}\n')
        sys.exit()

botPrefix = secret['prefix']
ex = Executor(secret['ssh']['enabled'], 
              secret['ssh']['host'], 
              secret['ssh']['user'], 
              secret['ssh']['keypath'])

# Initialize Channels
channels = []
if (secret['debug']):
    try:
        from channels.mattermost import Mattermost
        channels.append(Mattermost(secret['mattermostDev']['url'],
                                secret['mattermostDev']['verify'],
                                secret['mattermostDev']['user'],
                                secret['mattermostDev']['password'],
                                secret['mattermostDev']['scheme'],
                                secret['mattermostDev']['port'] ))
        print(f'>>> {bcolors.OKGREEN}Channel: MattermostDev ({secret["mattermostDev"]["url"]}) Initialized; working on Grafana...{bcolors.ENDC}')
    except Exception as e:
        print(f'>>> {bcolors.FAIL}Channel: MattermostDev ({secret["mattermostDev"]["url"]}) Failed.{bcolors.ENDC}\n\n{e}\n')
else:
    try:
        from channels.mattermost import Mattermost
        channels.append(Mattermost(secret['mattermost']['url'],
                                secret['mattermost']['user'],
                                secret['mattermost']['password'],
                                secret['mattermost']['scheme'],
                                secret['mattermost']['verifier'],
                                secret['mattermost']['port']))
        print(f'>>> {bcolors.OKGREEN}Channel: Mattermost ({secret["mattermost"]["url"]}) Initialized; working on MattermostDev...{bcolors.ENDC}')
    except Exception as e:
        print(f'>>> {bcolors.FAIL}Channel: Mattermost ({secret["mattermost"]["url"]}) Failed.{bcolors.ENDC}\n\n{e}\n')

# Initialize Integrations
try:
    from integrations.grafana import Grafana
    intGrafana = None
    intGrafana = Grafana(secret['grafana']['scheme'] + '://' + secret['grafana']['url'],
                         secret['grafana']['user'],
                         secret['grafana']['password'])
    print(f'>>> {bcolors.OKGREEN}Integration: Grafana Initialized; working on OpenShift...{bcolors.ENDC}')
except Exception as e:
    print(f'>>> {bcolors.FAIL}Integration: Grafana Failed.{bcolors.ENDC}\n\n{e}\n')
try:
    from integrations.openshift import OpenShift
    intOpenShift = None
    intOpenShift = OpenShift(ex)
    print(f'>>> {bcolors.OKGREEN}Integration: OpenShift Initialized; working on Kubernetes...{bcolors.ENDC}')
except Exception as e:
    print(f'>>> {bcolors.FAIL}Integration: OpenShift Failed.{bcolors.ENDC}\n\n{e}\n')
try:
    from integrations.kubernetes import Kubernetes
    intKubernetes = None
    intKubernetes = Kubernetes(secret['kubeContextDic']['kubeCxt'])
    placeHolder = (secret['kubeContextDic']['kubeCxt'])
    print(f'>>> {bcolors.OKGREEN}Integration: Kubernetes Initialized; working on Azure...{bcolors.ENDC}')
    print(f'>>> {bcolors.OKGREEN}Working on namespace: cluster {placeHolder} {bcolors.ENDC}')
  
except Exception as e:
    print(f'>>> {bcolors.FAIL}Integration: Kubernetes Failed.{bcolors.ENDC}\n\n{e}\n')
try:
    from integrations.msazure import MSAzure
    intMSAzure = None
    intMSAzure = MSAzure(secret['azure']['subscriptionId'],
                         secret['azure']['clientId'],
                         secret['azure']['clientSecret'],
                         secret['azure']['tenantId'])
    print(f'>>> {bcolors.OKGREEN}Integration: Azure Initialized; working on Help...{bcolors.ENDC}')
except Exception as e:
    print(f'>>> {bcolors.FAIL}Integration: Azure Failed.{bcolors.ENDC}\n\n{e}\n')

# Register Commands
try:
    from commands.help import Help
    cmdHelp = Help(botPrefix)
    for channel in channels:
        channel.subscribe(cmdHelp.reciever)
    print(f'>>> {bcolors.OKGREEN}Command: Help Initialized; working on Dice...{bcolors.ENDC}')
except Exception as e:
    print(f'>>> {bcolors.FAIL}Command: Help Failed.{bcolors.ENDC}\n\n{e}\n')
try:
    from commands.dice import Dice
    cmdDice = Dice(botPrefix)
    for channel in channels:
        channel.subscribe(cmdDice.reciever)
    print(f'>>> {bcolors.OKGREEN}Command: Dice Initialized; working on Alerts...{bcolors.ENDC}')
except Exception as e:
    print(f'>>> {bcolors.FAIL}Command: Dice Failed.{bcolors.ENDC}\n\n{e}\n')
try:
    from commands.alerts import Alerts
    cmdAlerts = Alerts(botPrefix, intGrafana)
    for channel in channels:
        channel.subscribe(cmdAlerts.reciever)
    print(f'>>> {bcolors.OKGREEN}Command: Alerts Initialized; working on Kubernetes...{bcolors.ENDC}')
except Exception as e:
    print(f'>>> {bcolors.FAIL}Command: Alerts Failed.{bcolors.ENDC}\n\n{e}\n')
try:
    from commands.k8s import K8s
    cmdK8s = K8s(botPrefix, intOpenShift, intKubernetes, intMSAzure)
    for channel in channels:
        channel.subscribe(cmdK8s.reciever)
    print(f'>>> {bcolors.OKGREEN}Command: Kubernetes Initialized; working on Executor{bcolors.ENDC}')
except Exception as e:
    print(f'>>> {bcolors.FAIL}Command: Kubernetes Failed.{bcolors.ENDC}\n\n{e}\n')
try:
    from commands.exec import Exec
    cmdExec = Exec(botPrefix, ex)
    for channel in channels:
        channel.subscribe(cmdExec.reciever)
    print(f'>>> {bcolors.OKGREEN}Command: Executor Initialized.{bcolors.ENDC}')
except Exception as e:
    print(f'>>> {bcolors.FAIL}Command: Executor Failed.{bcolors.ENDC}\n\n{e}\n')
try:
    from commands.elk import Elk
    cmdElk = Elk(botPrefix)
    for channel in channels:
        channel.subscribe(cmdElk.reciever)
    print(f'>>> {bcolors.OKGREEN}Command: Help Initialized; working on Elk...{bcolors.ENDC}')
except Exception as e:
    print(f'>>> {bcolors.FAIL}Command: Elk Failed.{bcolors.ENDC}\n\n{e}\n')

# Run
print(f'>>> {bcolors.HEADER}CF Guild Bot :: Initialized. Running All Channels..{bcolors.ENDC}')
for channel in channels:
    thread = threading.Thread(target=channel.connect, args=(asyncio.new_event_loop(),))
    thread.start()
