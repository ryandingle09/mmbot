import paramiko, warnings, subprocess

class Executor:
    def __init__(self, useSSH: bool, host: str = None, user: str = None, keypath: str = None):
        self.__useSSH = useSSH
        self.__sshHost = host
        self.__sshUser = user
        self.__sshKeypath = keypath
        if (useSSH):
            self.__initSSH()

    def __initSSH(self):
        warnings.filterwarnings(action='ignore',module='.*paramiko.*')
        self.__client = paramiko.SSHClient()
        self.__key = paramiko.RSAKey.from_private_key_file(self.__sshKeypath)
        self.__client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.__client.connect(hostname=self.__sshHost, username=self.__sshUser, pkey=self.__key)

    def __execSSH(self, command: str):
        _, stdout, stderr = self.__client.exec_command(command, get_pty=True)
        r = []
        errList = stderr.readlines()
        if (errList.__len__() > 0):
            r = errList
        else:
            r = stdout.readlines()
        return r

    def __execLocal(self, command: str):
        p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        r = []
        for line in p.stdout:
            r.append(line.decode('utf-8'))
        return r

    def run(self, command: str):
        if (self.__useSSH):
            return list(map(str.strip, self.__execSSH(command)))
        else:
            return list(map(str.strip, self.__execLocal(command)))
