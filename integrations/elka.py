import traceback

onboarding_commands = {
    'onboarding-success':'#### There are %s successfully onboarded today.',
    'onboarding-failed':'#### There are %s failed to onboard today.',
    'kyc-namescreening-status':'#### KYC Namescreenig is %s.',
    'kyc-risk-status':'#### KYC Risk is %s.',
    'otp-status':'#### OTP %s.',
    'otp-aws-status':'#### AWS Service is %s',
    'cif-creation-status':'#### CIF Creation %s',
    'face-liveness-status':'#### Face Liveness is %s.',
    'id-upload-status':'#### ID Upload is %s.',
    'account-summary-status':'#### Account Summary is %s.',
    'onboarding-batch-status':'#### Onboarding Batch has sucessfull %s today.'
}

transaction_commands = {
    'pesonet-actf':'#### Pesonet After Cutoff Transfer Failed for today is %s',
    'credit-tsf':'#### Credit to Suspense Failed Count for today is %s',
    'check-dfc':'#### Check Deposit Failed Count for today is %s',
    'failed-cr':'#### Failed Check Rejection Count for today is %s',
    'check-drf':'#### Check Deposit Return Failed Count for today is %s',
    'realease-hcf':'#### Release Hold Checks Failed Count for today is %s',
    'transaction-attempts':'#### Transaction Attempts Count for today is %s',
    'success-it':'#### Successful Internal Transfer Count for today is %s',
    'internal-tf':'#### Internal Transfer Failed Count for today is %s',
    'intraday-et':'#### Intraday External Transfer Count for today is %s',
    'pesonet-etf':'#### Failed Pesonet External Transfer Count for today is %s',
    'after-cpets':'#### After Cutoff Pesonet External Transfer Success Count for today is %s',
    'external-tlf':'#### External Transfer Limit Failed Count for today is %s',
    'debit-ss':'#### Debit from Suspense Success Count for today is %s',
    'debit-sf':'#### Debit from Suspense Failed Count for today is %s',
    'pesonet-ics':'#### Pesonet Inward Credit Success Count for today is %s',
    'pesonet-icf':'#### Pesonet Inward Credit Failed Count for today is %s',
    'pesonet-css':'#### Pesonet Credit to Suspense Success Count for today is %s',
    'pesonet-ir':'#### Pesonet Inward Reversal Count for today is %s',
    'settled-if':'#### Settled Inward Transactions Count for today is %s',
    'pesonet-aphf':'#### Pesonet Apply Permanent Hold Failed Count for today is %s',
    'check-ds':'#### Check Deposit Success Count for today is %s',
    'check-dar':'#### Check Deposit Attempt Reversal Count for today is %s',
    'check-dfch':'#### Check Deposit Failed Check Hold Count for today is %s',
    'check-fir':'#### Front Image - Rescans Count for today is %s',
    'check-rir':'#### Rear Image - Rescan Count for today is %s',
    'transaction-ci':'#### Connection Issues Count for today is %s',
    'transaction-se':'#### System Error Count for today is %s',
    'rejected-cd':'#### Rejected Check Deposit Count for today is %s',
    'instapay-stm':'#### Instapay Success Transactions Metric Count for today is %s',
    'instapay-ftm':'#### Instapay Failed Transactions Metric Count for today is %s',
    'check-drs':'#### Check Deposit Return Success Count for today is %s',
    'release-hcs':'#### Release Hold Checks Success Count for today is %s',
    'instapay-fvtm':'#### Instapay For Validation Transactions Metric Count for today is %s'
}

class Onboarding:

    def __init__(self, command):
        self.command = command
        self.onboarding_commands = {
            'onboarding-success': self.__get_onboarding_success(),
            'onboarding-failed': self.__get_onboarding_failed(),
            'kyc-namescreening-status': self.__get_kyc_namescreening(),
            'kyc-risk-status': self.__get_kyc_risk(),
            'otp-status': self.__get_otp(),
            'otp-aws-status': self.__get_otp_aws(),
            'cif-creation-status': self.__get_cif_creation(),
            'face-liveness-status': self.__get_face_liveness(),
            'id-upload-status': self.__get_id_upload(),
            'account-summary-status': self.__get_account_summary(),
            'onboarding-batch-status': self.__get_onboarding_batch()
        }

    def __get_onboarding_success(self):
        return onboarding_commands[self.command] % '1,024'
    
    def __get_onboarding_failed(self):
        return onboarding_commands[self.command] % '424'

    def __get_kyc_namescreening(self):
        return onboarding_commands[self.command] % 'UP'

    def __get_kyc_risk(self):
        return onboarding_commands[self.command] % 'UP'

    def __get_otp(self):
        return onboarding_commands[self.command] % 'Has no error detected'

    def __get_otp_aws(self):
        return onboarding_commands[self.command] % 'UP'

    def __get_cif_creation(self):
        return onboarding_commands[self.command] % 'UP'

    def __get_kyc_namescreening(self):
        return onboarding_commands[self.command] % 'UP'

    def __get_face_liveness(self):
        return onboarding_commands[self.command] % 'UP'

    def __get_id_upload(self):
        return onboarding_commands[self.command] % 'UP'

    def __get_account_summary(self):
        return onboarding_commands[self.command] % 'UP'

    def __get_onboarding_batch(self):
        return onboarding_commands[self.command] % 'Done'

    def get_result(self):
        return self.onboarding_commands[self.command]

class Transaction:

    def __init__(self, command):
        self.command = command
        self.transaction_commands = {
            'pesonet-actf':self.__get_pesonet_actf(),
            'credit-tsf':self.__get_credit_tsf(),
            'check-dfc':self.__get_check_dfc(),
            'failed-cr':self.__get_failed_cr(),
            'check-drf':self.__get_check_drf(),
            'realease-hcf':self.__get_release_hcf(),
            'transaction-attempts':self.__get_transaction_attemps(),
            'success-it':self.__get_success_it(),
            'internal-tf':self.__get_internal_tf(),
            'intraday-et':self.__get_intraday_et(),
            'pesonet-etf':self.__get_pesonet_etf(),
            'after-cpets':self.__get_after_cpets(),
            'external-tlf':self.__get_external_tlf(),
            'debit-ss':self.__get_debit_ss(),
            'debit-sf':self.__get_debit_sf(),
            'pesonet-ics':self.__get_pesonet_ics(),
            'pesonet-icf':self.__get_pesonet_icf(),
            'pesonet-css':self.__get_pesonet_css(),
            'pesonet-ir':self.__get_pesonet_ir(),
            'settled-if':self.__get_settled_if(),
            'pesonet-aphf':self.__get_pesonet_aphf(),
            'check-ds':self.__get_check_ds(),
            'check-dar':self.__get_check_dar(),
            'check-dfch':self.__get_check_dfch(),
            'check-fir':self.___get_check_fir(),
            'check-rir':self.__get__check_rir(),
            'transaction-ci':self.__get_transaction_ci(),
            'transaction-se':self.__get_transaction_se(),
            'rejected-cd':self.__get_rejected_cd(),
            'instapay-stm':self.__get_instapay_stm(),
            'instapay-ftm':self.__get_instapay_ftm(),
            'check-drs':self.__get_check_drs(),
            'release-hcs':self.__get_release_hcs(),
            'instapay-fvtm':self.__get_instapay_fvtm()
        }

    def __get_pesonet_actf(self):
        return transaction_commands[self.command] % '0'
    
    def __get_credit_tsf(self):
        return transaction_commands[self.command] % '0'

    def __get_check_dfc(self):
        return transaction_commands[self.command] % '0'
    
    def __get_failed_cr(self):
        return transaction_commands[self.command] % '0'
    
    def __get_check_drf(self):
        return transaction_commands[self.command] % '0'

    def __get_release_hcf(self):
        return transaction_commands[self.command] % '0'

    def __get_transaction_attemps(self):
        return transaction_commands[self.command] % '0'

    def __get_success_it(self):
        return transaction_commands[self.command] % '500'

    def __get_internal_tf(self):
        return transaction_commands[self.command] % '460'

    def __get_intraday_et(self):
        return transaction_commands[self.command] % '0'
    
    def __get_pesonet_etf(self):
        return transaction_commands[self.command] % '0'

    def __get_after_cpets(self):
        return transaction_commands[self.command] % '0'

    def __get_external_tlf(self):
        return transaction_commands[self.command] % '0'

    def __get_debit_ss(self):
        return transaction_commands[self.command] % '0'

    def __get_debit_sf(self):
        return transaction_commands[self.command] % '0'

    def __get_pesonet_ics(self):
        return transaction_commands[self.command] % '0'

    def __get_pesonet_icf(self):
        return transaction_commands[self.command] % '0'

    def __get_pesonet_css(self):
        return transaction_commands[self.command] % '0'

    def __get_pesonet_ir(self):
        return transaction_commands[self.command] % '0'
    
    def __get_settled_if(self):
        return transaction_commands[self.command] % '0'
    
    def __get_pesonet_aphf(self):
        return transaction_commands[self.command] % '0'

    def __get_check_ds(self):
        return transaction_commands[self.command] % '0'

    def __get_check_dar(self):
        return transaction_commands[self.command] % '0'
    
    def __get_check_dfch(self):
        return transaction_commands[self.command] % '0'

    def ___get_check_fir(self):
        return transaction_commands[self.command] % '0'

    def __get__check_rir(self):
        return transaction_commands[self.command] % '0'

    def __get_transaction_ci(self):
        return transaction_commands[self.command] % '0'

    def __get_transaction_se(self):
        return transaction_commands[self.command] % '0'

    def __get_rejected_cd(self):
        return transaction_commands[self.command] % '0'

    def __get_rejected_cd(self):
        return transaction_commands[self.command] % '0'

    def __get_instapay_stm(self):
        return transaction_commands[self.command] % '0'

    def __get_instapay_ftm(self):
        return transaction_commands[self.command] % '0'

    def __get_check_drs(self):
        return transaction_commands[self.command] % '0'

    def __get_release_hcs(self):
        return transaction_commands[self.command] % '0'
    
    def __get_instapay_fvtm(self):
        return transaction_commands[self.command] % '0'

    def get_result(self):
        return self.transaction_commands[self.command]

