from kubernetes import client, config

class Kubernetes:
    def __init__(self, kubeContext: str ):
        self.__namespacesSystem = ['default','istio-system','kube-public','kube-system','logging','management-infra', 
                                   'openshift','openshift-infra','openshift-node','openshift-web-console']
        self._kubeCxt = kubeContext                        
        config.load_kube_config(context=self._kubeCxt)

    def getNamespaces(self):
        r = []
        namespaces = client.CoreV1Api().list_namespace().items
        for namespace in namespaces:
            if namespace.metadata.name not in self.__namespacesSystem:
                r.append(namespace.metadata.name)
        return r
    
    def getProjects(self):
        r = []
        namespaces = client.CoreV1Api().list_namespace().items
        for namespace in namespaces:
            if namespace.metadata.name not in self.__namespacesSystem:
                total_replicas = 0
                total_available = 0
                for j in client.AppsV1Api().list_namespaced_deployment(namespace.metadata.name).items:
                    total_replicas += j.spec.replicas if j.spec.replicas is not None else 0
                    total_available += j.status.ready_replicas if j.status.ready_replicas is not None else 0
                r.append({'name':namespace.metadata.name, 'available':total_available, 'replicas':total_replicas})
        return r

    def getNodes(self):
        r = []
        nodes = client.CoreV1Api().list_node().items
        for node in nodes:
            conditions = []
            for item in node.status.conditions:
                if item.status == 'True':
                    conditions.append(item.type)
                elif item.status == 'Unknown':
                    conditions.append('Not Ready')
                    break
            r.append({'name': node.metadata.name, 'conditions': ' '.join(conditions)})
        return r

    def getDeployments(self, namespace: str):
        r = []
        deployments = client.AppsV1Api().list_namespaced_deployment(namespace).items
        for deployment in deployments:
            if deployment.status.available_replicas is not None:
                r.append({'name':deployment.metadata.name, 
                          'available': deployment.status.available_replicas, 
                          'replicas': deployment.spec.replicas})
        return r

    def getDeploymentsFast(self, namespace: str):
        return list(i.metadata.name for i in client.AppsV1Api().list_namespaced_deployment(namespace).items)

    def getImage(self, namespace:str, deployment: str):
        deployment = client.AppsV1Api().read_namespaced_deployment(deployment, namespace)
        return deployment.spec.template.spec.containers[0].image

    def getEnvVars(self, namespace: str, deployment: str):
        r = []
        deployment = client.AppsV1Api().read_namespaced_deployment(deployment, namespace)
        envVars = deployment.spec.template.spec.containers[0].env
        if (envVars is not None):
            for var in envVars:
                if ('pass' not in var.name.lower() and
                    'secret' not in var.name.lower() and
                    'accesskey' not in var.name.lower()):
                    r.append({'key': var.name, 'value': var.value})
        return r

    def getEndpoints(self, namespace: str, deployment: str):
        r = []
        deployment = client.CoreV1Api().read_namespaced_endpoints(deployment, namespace)
        address = deployment.subsets[0].addresses[0].ip
        for endpoint in deployment.subsets[0].ports:
            r.append({'address':address, 'name': endpoint.name, 'port': endpoint.port, 'protocol': endpoint.protocol})
        return r

    def getLogs(self, namespace: str, deployment:str, limit: int):
        r = []
        for pod in client.CoreV1Api().list_namespaced_pod(namespace).items:
            if (deployment in pod.metadata.name):
                r.append(client.CoreV1Api().read_namespaced_pod_log(pod.metadata.name, namespace, tail_lines=limit, pretty=True))
        return r

    def scaleDeployment(self, namespace: str, deployment: str, replicas: int):
        try:
            deploy = client.AppsV1Api().read_namespaced_deployment_scale(deployment, namespace)
            deploy.spec.replicas = replicas
            client.AppsV1Api().patch_namespaced_deployment_scale(deployment, namespace, deploy)
            return str(True)
        except Exception as e:
            return str(e)

    def getPods(self, namespace:str, args):
        r = []
        coreApi = client.CoreV1Api()
        ret = coreApi.list_pod_for_all_namespaces(watch=False)
        for pod in ret.items:
            if pod.metadata.namespace == namespace:
                if len(args) != 0:
                    if 'running' in str(args[0]).lower() and str(pod.status.phase).lower() == 'running':
                        r.append({'name': pod.metadata.name, 'status':pod.status.phase })
                    if 'pending' in str(args[0]).lower() and str(pod.status.phase).lower() == 'pending':
                        r.append({'name': pod.metadata.name, 'status':pod.status.phase })
                    if 'succeeded' in str(args[0]).lower() and str(pod.status.phase).lower() == 'succeeded':
                        r.append({'name': pod.metadata.name, 'status':pod.status.phase })
                    if 'failed' in str(args[0]).lower() and str(pod.status.phase).lower() == 'failed':
                        r.append({'name': pod.metadata.name, 'status':pod.status.phase })
                    if 'unknown' in str(args[0]).lower() and str(pod.status.phase).lower() == 'unknown':
                        r.append({'name': pod.metadata.name, 'status':pod.status.phase })
                else:
                    r.append({'name': pod.metadata.name, 'status':pod.status.phase })
        return r

    def getPodLogs(self, namespace:str, name):
        try: 
            coreApi = client.CoreV1Api()
            api_response = coreApi.read_namespaced_pod_log(name, namespace, tail_lines=90, pretty='true')
            return str(api_response)

        except Exception as e:
            return "Exception when calling coreApi->read_namespaced_pod_log. Please report to DevOps: %s\n" % e

    def getPodDescription(self, namespace:str, name):
        try: 
            coreApi = client.CoreV1Api()
            api_response = coreApi.read_namespaced_pod_status(name, namespace, pretty='true')
            return str(api_response)

        except Exception as e:
            return "Exception when calling coreApi->read_namespaced_pod_status. Please report to DevOps: %s\n" % e


