from executor import Executor

class OpenShift:
    def __init__(self, exec: Executor):
        self.__exec = exec

    def getCluster(self):
        command = 'oc project | cut -d/ -f3 | cut -d: -f1'
        return self.__exec.run(command)
