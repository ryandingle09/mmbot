import requests, json

class Grafana:
    def __init__(self, grafanaUrl: str, grafanaUser: str, grafanaPassword: str):
        self.__grafanaUrl = grafanaUrl
        self.__grafanaUser = grafanaUser
        self.__grafanaPassword = grafanaPassword
        self.updateAlertList()

    def __request(self, endpoint: str):
        return requests.get(f'{self.__grafanaUrl}/api/{endpoint}', auth=(self.__grafanaUser, self.__grafanaPassword))
        
    __alertList = []
    def updateAlertList(self):
        r = self.__request('alerts')
        self.__alertList = json.loads(r.text)

    def listAlerts(self):
        r = []
        for alert in self.__alertList:
            if (alert['state'] == 'no_data'):
                r.append({'state':'warning', 'source':'Grafana', 'name':alert['name'], 'metric':'n/a', 'value':'No Data'})
            elif (alert['state'] == 'ok'):
                r.append({'state':'ok', 'source':'Grafana', 'name':alert['name'], 'metric':'n/a', 'value':'n/a'})
            elif (alert['state'] == 'alerting'):
                if (alert['evalData'] is not None):
                    if ('evalMatches' in alert['evalData']):
                        for evalMatch in alert['evalData']['evalMatches']:
                            r.append({'state':'alert', 'source':'Grafana', 'name':alert['name'], 'metric':evalMatch['metric'], 'value':evalMatch['value']})
                    elif ('error' in alert['evalData']):
                        r.append({'state':'alert', 'source':'Grafana', 'name':alert['name'], 'metric':alert['evalData']['error'], 'value':'Error'})
        return r
