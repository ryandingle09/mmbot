from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.compute import ComputeManagementClient

class MSAzure:
    def __init__(self, subscriptionId: str, clientId: str, clientSecret: str, tenantId: str):
        self.__subscriptionId = subscriptionId
        self.__credentials = ServicePrincipalCredentials(client_id=clientId, secret=clientSecret, tenant=tenantId)

    def listResources(self):
        resource_client = ResourceManagementClient(self.__credentials, self.__subscriptionId)
        return list(r.name for r in resource_client.resource_groups.list())

    def getResourceGroupStatus(self, resourceGroup: str):
        r = []
        resource_client = ResourceManagementClient(self.__credentials, self.__subscriptionId)
        vm_client = ComputeManagementClient(self.__credentials, self.__subscriptionId)
        fltr = "resourceType eq 'Microsoft.Compute/virtualMachines'"
        try:
            for resource in resource_client.resources.list_by_resource_group(resourceGroup, filter=fltr):
                vm = vm_client.virtual_machines.instance_view(resourceGroup, resource.name)
                r.append({'name':resource.name, 'state':vm.statuses[1].code})
        except Exception as e:
            print(e)
        return r

    def getBilling(self, resource: str, startDate: str, endDate: str):
        pass
