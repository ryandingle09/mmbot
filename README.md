# **Vader**, the DevOps CF/Monitoring Guild Bot

Vader is chatbot that can service any number of channels (currently only Mattermost) and connect to any number of integrations via commands. 

## Overview

``` text
------------------       ----------------      ---------------
| Channels       |       | Commands     |      | Kubernetes  |
|  - Mattermost  | sends |  - !exec     | <==> | Integration |
|  - Facebook?   | <===> |  - !k8s      |      ---------------
|  - Telegram?   |       |  - !alerts   | 
------------------       |  - etc..     |      ---------------
                         |              | <==> | Grafana     |
                         |              |      | Integration |
                         ----------------      ---------------
```

If you check out `bot.py`, you will see Vader's initialization sequence. First, the secrets are loaded from a json file. Next, the channels are collected to a list. Each channel has two important methods within it: the subscribe event that fires every time the channel receives a message, and a send function that allows the bot to reply.

Afterwards, the Integrations are loaded. Each integration connects to a specific application that is external from the bot. This includes SSH, Bash, Kubernetes, and others. Note that secrets are always passed during initialization.

Lastly, each command is initialized. Note that each of these classes takes one or more integration objects that was initialized earlier, as well as subscribing to all available channels in order to recieve messages and reply to them.

## Prerequisites

* Python >= 3.7.3
* Pip >= 19.1.1

Deployment requirements:

* Python & Pip, same as above
* PM2 >= 3.5.1

## Installing

1. Git Clone to your workstation
2. Duplicate the `secret-template.json` file and rename it to `secret-dev.json`. Fill it up with secrets.
3. OPTIONAL: Create a new Python Environment by running `python3 -m venv /path/to/new/env`, then `source /path/to/new/env/bin/activate`
4. Run `python3 -m pip install -r requirements.txt`
5. To start the bot, run `python3 bot.py secret-dev.json`

## Deployment

1. Git Clone to target deployment machine
2. Duplicate the `secret-template.json` file and rename it to `secret-prod.json`. Fill it up with secrets.
3. Run `python3 -m pip install pex`
4. Run `python3 -m pex . -r requirements.txt -o dist/CFGuildBot.pex -e bot --disable-cache -v`
5. To start the bot, run `pm2 start ./dist/CFGuildBot.pex -- secret-prod.json`

### Deployment w/o Internet Connection
1. Deploy on an identical machine with an internet connection, up to step 4 above.
2. SCP the created pex file to the target machine w/o internet connection, as well as the secrets json file.
3. To start the bot, run `./CFGuildBot.pex secret-prod.json &`. It is recommended to setup a cron job to ensure uptime of the bot.

## Contributing

When adding more commands, follow the guidelines below:

* Always create a new branch for each feature/fix to keep the `master` branch always ready for deployment to production. When the feature/fix is finished, make a merge request with the appropriate approvers.
* Checkout from (and Merge to) the `develop` branch. It will be merged to the `master` branch every release.
* Start adding functionality at the integration level first. These changes should be fully disconnected from the command so that it can be reused easily.
* When implementing the command itself, don't forget to add your command and its aliases at the `reciever` function.
* Don't forget to document your command by modifying `commands/help.py`. Remember that undocumented features are just bugs in disguise.

## TODO

* Most of the commands are listed in `commands\help.py` with either a :white_check_mark: for **done** or a :building_construction: for **being developed**.
* Priority: Develop the Role Management commands to limit access to commands. The `!exec` command can be used for malicious purposes.
* Add more Channels
