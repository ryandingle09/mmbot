import abc
from channels.message import Message

# Channel Interface
class IChannel(abc.ABC):
    @abc.abstractmethod
    def subscribe(self, callback):
        pass

    @abc.abstractmethod
    def send(self, message: Message):
        pass

    @abc.abstractmethod
    def connect(self):
        pass
