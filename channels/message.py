class Message:
    # Message Model

    def __init__(self, raw: str):
        self.raw = raw
        self.__source = None
        self.__eventName = None
        self.__channel = None
        self.__channelId = None
        self.__sender = None
        self.__senderId = None
        self.__content = None
        self.__contentId = None

    @property
    def source(self):
        return self.__source

    @source.setter
    def source(self, Source: str):
        self.__source = Source

    @property
    def eventName(self):
        return self.__eventName
    
    @eventName.setter
    def eventName(self, Event: str):
        self.__eventName = Event

    @property
    def channel(self):
        return self.__channel
    
    @channel.setter
    def channel(self, Channel: str):
        self.__channel = Channel

    @property
    def channelId(self):
        return self.__channelId
    
    @channelId.setter
    def channelId(self, ChannelId: str):
        self.__channelId = ChannelId

    @property
    def sender(self):
        return self.__sender
    
    @sender.setter
    def sender(self, Sender: str):
        self.__sender = Sender

    @property
    def senderId(self):
        return self.__senderId
    
    @senderId.setter
    def senderId(self, SenderId: str):
        self.__senderId = SenderId

    @property
    def content(self):
        return self.__content
    
    @content.setter
    def content(self, Content: str):
        self.__content = Content

    @property
    def contentId(self):
        return self.__contentId
    
    @contentId.setter
    def contentId(self, ContentId: str):
        self.__contentId = ContentId
