from channels.channel import IChannel
from channels.message import Message
from mattermostdriver import Driver
import asyncio, json, threading
from role import Role

class Mattermost(IChannel):
    def __init__( self,  url: str, verify: bool ,login_id: str, password: str, scheme: str = 'https',port: int = 8065 ):
        self.__url = url
        self.__verify = verify
        self.__driver = Driver({
            'url': url,
            'login_id': login_id,
            'password': password,
            'scheme': scheme,
            'verify': self.__verify,
            'port': port
        })
        self.__driver.login()
        self.__me = self.__driver.users.get_user(user_id='me')
        self.__subscribers = []
    
    @asyncio.coroutine
    def __my_event_handler(self, message):
        m = json.loads(message)
        if 'event' in m:
            r = Message(message)
            r.source = f'MATTERMOST/{self.__url}'
            r.eventName = m['event']

            if m['event'] == 'posted':
                p = json.loads(m['data']['post'])
                r.channel = m['data']['channel_name']
                r.channelId = p['channel_id']
                r.sender = m['data']['sender_name']
                r.senderId = p['user_id']
                r.content = p['message']
                r.contentId = p['id']

                rs = Role(m['data']['sender_name'])
                role, group, grouping = rs.check_my_role()

                r.roles = role
                r.groups = group
                r.groupings = grouping

            self.__notify(r)

    def __daemonizer(self, target, args):
        thread = threading.Thread(target=target, args=args)
        thread.daemon = True
        thread.start()

    def subscribe(self, callback):
        self.__subscribers.append(callback)

    def __notify(self, m: Message):
        for subscriber in self.__subscribers:
            self.__daemonizer(subscriber, (m, self.send))

    def send(self, message: Message, senderId: str = None):
        if (senderId is not None):
            participants = []
            participants.append(self.__me['id'])
            participants.append(senderId)
            channel = self.__driver.channels.create_direct_message_channel(options=participants)
            self.__driver.posts.create_post(options={
                'channel_id': channel['id'],
                'message': message.content
            })
        else:
            self.__driver.posts.create_post(options={
                'channel_id': message.channelId,
                'message': message.content
            })

    def connect(self, loop):
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.__driver.init_websocket(self.__my_event_handler))
