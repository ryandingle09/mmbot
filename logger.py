import requests
from endpoints import log_endpoint

class Logger:

    def send_logs(action, username):
        requests.post(log_endpoint, data={'action': action, 'username': username})