from channels.message import Message
import shlex, bcolors
from integrations.elka import Onboarding, Transaction, onboarding_commands, transaction_commands
from logger import Logger

class Elk():
    def __init__(self, botPrefix: str):
        self.__botPrefix = botPrefix
        self.__cmdPrefix = f'{botPrefix}elk'

    def reciever(self, m: Message, reply):

        if (m.content is not None):
            try:
                content = shlex.split(m.content)
            except:
                return
            if (m.content.lower().startswith(self.__cmdPrefix)):
                try:
                    content.remove(self.__cmdPrefix)
                except:
                    return
                if (content.__len__() == 0):
                    content.append('?')
                subcommand = content.pop(0)

                Logger.send_logs(m.content, m.sender)

                print(f'>>> {bcolors.OKBLUE}Command: Help.{subcommand} recieved from {m.source}/{m.channel}/{m.sender}{bcolors.ENDC}')
                print(m.roles)
                print(m.groups)

                r = Message('CFGUILDBOT/k8s')
                r.channelId = m.channelId
                r.channel = m.channel
                
                command = subcommand.lower()

                if str(command) not in m.roles:
                    self.role_checker(str(command), m, reply)

                else:

                    if command in list(onboarding_commands):
                        onboarding_class = Onboarding(command)
                        r.content = onboarding_class.get_result()

                    elif command in list(transaction_commands):
                        transaction_class = Transaction(command)
                        r.content = transaction_class.get_result()
                    
                    else:
                        r.content = 'Command not found :( \n'

                        if 'production-onboarding' in m.groups:
                            r.content+=f'`{"!help elk-o|eo ".ljust(40, "·")}` :building_construction: Production related commands for onboarding checking\n'

                        if 'production-transaction' in m.groups:
                            r.content+=f'`{"!help elk-t|et ".ljust(40, "·")}` :building_construction:  Production related commands for transaction checking\n'
                
                reply(r)
    
    def role_checker(self, role, m, reply, DM: bool = False):
        if str(role) not in m.roles:
            r = Message('CFGUILDBOT/help')
            r.channelId = m.channelId
            r.channel = m.channel
            r.content = ':warning: You do not have permission to perform this command.'
            reply(r, m.senderId if DM else None)
    
    def group_checker(self, group, m, reply, DM: bool = False):
        if str(group) not in m.groups:
            r = Message('CFGUILDBOT/help')
            r.channelId = m.channelId
            r.channel = m.channel
            r.content = ':warning: You do not have permission to perform this command.'
            reply(r, m.senderId if DM else None)