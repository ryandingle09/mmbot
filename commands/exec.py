from channels.message import Message
from executor import Executor
import shlex, bcolors
from logger import Logger

class Exec:
    def __init__(self, botPrefix: str, exec: Executor):
        self.__botPrefix = botPrefix
        self.__cmdPrefix = f'{botPrefix}exec'
        self.__exec = exec

    def reciever(self, m: Message, reply):
        if (m.content is not None):
            try:
                content = shlex.split(m.content)
            except:
                return
            if (m.content.lower().startswith(self.__cmdPrefix)):
                try:
                    content.remove(self.__cmdPrefix)
                except:
                    return
                if (content.__len__() == 0):
                    content.append('top -bn 1 -o %CPU | head -n 13')
                execCommand = ' '.join(map(str, content))

                Logger.send_logs(m.content, m.sender)

                print('Command is : %s' % execCommand)

                if 'exec' not in m.roles:
                    r = Message('CFGUILDBOT/exec')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content = ':warning: You do not have permission to perform `exec` commands.'
                    reply(r)

                    return

                print(f'>>> {bcolors.OKBLUE}Command: Exec: {execCommand} recieved from {m.source}/{m.channel}/{m.sender}{bcolors.ENDC}')
                try:
                    r = Message('CFGUILDBOT/exec')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content = '```text\n' + '\n'.join(self.__exec.run(execCommand)) + '\n```'
                    reply(r)
                    print(f'>>> {bcolors.OKGREEN}Command: Exec: {execCommand} success.{bcolors.ENDC}')
                except Exception as e:
                    print(f'>>> {bcolors.FAIL}Command: Exec: {execCommand} failed.\n\n{e}\n{bcolors.ENDC}')
