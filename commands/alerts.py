from channels.message import Message
from integrations.grafana import Grafana
import shlex, bcolors
from logger import Logger

class Alerts:
    def __init__(self, botPrefix: str, intGrafana: Grafana = None):
        self.__botPrefix = botPrefix
        self.__cmdPrefix = f'{botPrefix}alerts'
        if (intGrafana is not None):
            self.__intGrafana = intGrafana

    def reciever(self, m: Message, reply):
        if (m.content is not None):
            try:
                content = shlex.split(m.content)
            except:
                return
            if (m.content.lower().startswith(self.__cmdPrefix)):
                try:
                    content.remove(self.__cmdPrefix)
                except:
                    return
                if (content.__len__() == 0):
                    content.append('list')
                subcommand = content.pop(0)

                Logger.send_logs(m.content, m.sender)

                print(f'>>> {bcolors.OKBLUE}Command: Alerts.{subcommand} recieved from {m.source}/{m.channel}/{m.sender}{bcolors.ENDC}')
                # Switch to subcommands HERE

                if 'alerts' not in m.roles:
                    r = Message('CFGUILDBOT/alerts')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content = ':warning: You do not have permission to perform this command.'
                    reply(r)
                    
                    return

                if (subcommand.lower() in ['?', 'h', 'help']):
                    r = Message('CFGUILDBOT/k8s')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content = 'Command not found. Did you mean `!help alerts`?'
                    print(f'>>> {bcolors.OKGREEN}Command: K8s.help success.{bcolors.ENDC}')
                    reply(r)
                elif (subcommand.lower() == 'list'):
                    self.listAlerts(content, m, reply)
                elif (subcommand.lower() in ['s', 'subscribe']):
                    self.subscribeToAlert(content, m, reply)
                elif (subcommand.lower() in ['u', 'unsubscribe']):
                    self.unsubscribeToAlert(content, m, reply)
                else:
                    r = Message('CFGUILDBOT/alerts')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content = 'Command not found :('
                    print(f'>>> {bcolors.FAIL}Command: Alerts.{subcommand} not found.{bcolors.ENDC}')
                    reply(r)

    def listAlerts(self, args, m, reply):
        # Setup Message
        r = Message('CFGUILDBOT/alerts')
        r.content = f'@{m.sender} List of Alerts (Filters Under Construction)\n\n' # TODO: as of..?
        r.content += f'| Source | Alert | Metric | Value |\n'
        r.content += f'|:------ |:----- | ------:| -----:|\n'
        r.channelId = m.channelId
        r.channel = m.channel
        # TODO: Setup Filter HERE
        # Grafana Integration
        self.__intGrafana.updateAlertList()
        for alert in self.__intGrafana.listAlerts():
            # TODO: Apply Filter HERE
            icon = ''
            if (alert['state'] == 'warning'): 
                icon = ':warning:'
            elif (alert['state'] == 'ok'): 
                icon = ':white_check_mark:'
            elif (alert['state'] == 'alert'): 
                icon = ':no_entry:'
            r.content += f"|{alert['source']}|{icon} {alert['name']}|{alert['metric']}|{alert['value']}|\n"
        print(f'>>> {bcolors.OKGREEN}Command: Alerts.list success.{bcolors.ENDC}')
        reply(r)

    def subscribeToAlert(self, args, m, reply):
        r = Message('CFGUILDBOT/alerts')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content = 'Alerts Subscription Service :warning: Under Construction. :warning: sorry~'
        reply(r)

    def unsubscribeToAlert(self, args, m, reply):
        r = Message('CFGUILDBOT/alerts')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content = 'Alerts Subscription Service :warning: Under Construction. :warning: sorry~'
        reply(r)
