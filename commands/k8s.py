from channels.message import Message
from integrations.openshift import OpenShift
from integrations.kubernetes import Kubernetes
from integrations.msazure import MSAzure
import shlex, bcolors, warnings
from logger import Logger

class K8s():
    def __init__(self, botPrefix: str, intOpenShift: OpenShift = None,
                 intKubernetes: Kubernetes = None, intMSAzure: MSAzure = None):
        warnings.filterwarnings(action='ignore',module='.*connectionpool.*')
        self.__botPrefix = botPrefix
        self.__cmdPrefix = f'{botPrefix}k8s'
        if (intOpenShift is not None):
            self.__intOpenShift = intOpenShift
        if (intKubernetes is not None):
            self.__intKubernetes = intKubernetes
            self.__projects = list(map(str.lower, self.__intKubernetes.getNamespaces()))
        if (intMSAzure is not None):
            self.__intMSAzure = intMSAzure
            self.__resourceGroups = list(map(str.lower, self.__intMSAzure.listResources()))

    def reciever(self, m: Message, reply):
        if (m.content is not None):
            try:
                content = shlex.split(m.content)
            except:
                return
            if (m.content.lower().startswith(self.__cmdPrefix)):
                try:
                    content.remove(self.__cmdPrefix)
                except:
                    return
                if (content.__len__() == 0):
                    content.append('projects')
                subcommand = content.pop(0)

                Logger.send_logs(m.content, m.sender)

                print(f'>>> {bcolors.OKBLUE}Command: K8s.{subcommand} recieved from {m.source}/{m.channel}/{m.sender}{bcolors.ENDC}')
                # Switch to subcommands HERE
                if subcommand.lower() in self.__projects:
                    namespaceMaybe = subcommand.lower()
                else:
                    namespaceMaybe = next((s for s in self.__projects if subcommand.lower() in s), None)
                resourceGroupMaybe = None # next((s for s in self.__resourceGroups if subcommand.lower() in s), None)
                if (subcommand.lower() in ['?', 'h', 'help']):
                    r = Message('CFGUILDBOT/k8s')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content = 'Command not found. Did you mean `!help k8s`?'
                    print(f'>>> {bcolors.OKGREEN}Command: K8s.help success.{bcolors.ENDC}')
                    reply(r)
                elif (subcommand.lower() in ['c', 'cluster']):
                    self.getCluster(content, m, reply)
                elif (subcommand.lower() in ['p', 'projects']):
                    self.getProjects(content, m, reply)
                elif (subcommand.lower() in ['n', 'nodes']):
                    self.getNodes(content, m, reply)
               
                elif (namespaceMaybe is not None):
                    if (content.__len__() == 0):
                        content.append('deploy')
                    subcommand = content.pop(0)
                    if (subcommand.lower() in ['d', 'deploy', 'deployments']):
                        if 'deployments' not in m.groups:
                            self.role_checker(namespaceMaybe, 'deploy', m, reply)
                        else:
                            self.getDeployments(namespaceMaybe, content, m, reply)
                    elif (subcommand.lower() in ['i', 'image']):
                        if 'image' not in m.groups:
                            self.role_checker(namespaceMaybe, 'image', m, reply)
                        else:
                            self.getImage(namespaceMaybe, content, m, reply)
                    elif (subcommand.lower() in ['p', 'pods']):
                        if 'pods' not in m.groups:
                            self.role_checker(namespaceMaybe, 'pods', m, reply)
                        else:
                            self.getPods(namespaceMaybe, content, m, reply)
                    elif (subcommand.lower() in ['env']):
                        if 'deployments' not in m.groups:
                            self.role_checker(namespaceMaybe, 'env', m, reply)
                        else:
                            self.getEnvVars(namespaceMaybe, content, m, reply)
                    elif (subcommand.lower() in ['end', 'endpoints']):
                        if 'deployments' not in m.groups:
                            self.role_checker(namespaceMaybe, 'end', m, reply)
                        else:
                            self.getEndpoints(namespaceMaybe, content, m, reply)
                    elif (subcommand.lower() in ['l', 'logs']):
                        if 'deployments' not in m.groups:
                            self.role_checker(namespaceMaybe, 'logs', m, reply)
                        else:
                            self.getLogs(namespaceMaybe, content, m, reply)
                    elif (subcommand.lower() in ['s', 'scale']):
                        if 'deployments' not in m.groups or 'pods' not in m.groups:
                            self.role_checker(namespaceMaybe, 'scale', m, reply)
                        else:
                            self.scaleDeployments(namespaceMaybe, content, m, reply)
                    elif (subcommand.lower() in ['sa', 'scale-all']):
                        if 'deployments' not in m.groups or 'pods' not in m.groups:
                            self.role_checker(namespaceMaybe, 'scale', m, reply)
                        else:
                            self.scaleAll(namespaceMaybe, content, m, reply)
                elif (resourceGroupMaybe is not None):
                    if (content.__len__() == 0):
                        content.append('status')
                    subcommand = content.pop(0)
                    if (subcommand.lower() in ['s', 'status']):
                        self.getRGStatus(resourceGroupMaybe, content, m, reply)
                    elif (subcommand.lower() in ['b', 'bill', 'billing']):
                        pass
                else:
                    r = Message('CFGUILDBOT/k8s')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content = 'Command not found :('
                    print(f'>>> {bcolors.FAIL}Command: K8s.{subcommand} not found.{bcolors.ENDC}')
                    reply(r)

    def role_checker(self, namespace, role, m, reply):
        if str(role) not in m.roles:
            r = Message('CFGUILDBOT/k8s')
            r.channelId = m.channelId
            r.channel = m.channel
            r.content = ':warning: You do not have permission to perform this command.'
            reply(r)

    def getCluster(self, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'This bot\'s active cluster context is `{self.__intOpenShift.getCluster()[0]}`, @{m.sender}.'
        reply(r)

    def getProjects(self, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content = f'One moment; running `!k8s projects`'
        reply(r)
        c1 =f'Here\'s your list of namespaces, @{m.sender}:\n\n```\n'
        c2 = 'These namespaces are empty (0/0) :\n\n```\n'
        ctr1 = 0
        ctr2 = 0
        self.__projects = []
        print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
        for project in self.__intKubernetes.getProjects():
            self.__projects.append(project['name'])
            if (project['available'] < 1):
                c2+=f'{project["name"]} ({project["available"]}/{project["replicas"]})'.ljust(30)
                ctr2+= 1
            else:
                c1+=f'{project["name"]} ({project["available"]}/{project["replicas"]})'.ljust(30)
                ctr1+= 1
            if (ctr2 == 4):
                c2+= '\n'
                ctr2 = 0
            if (ctr1 == 4):
                c1+= '\n'
                ctr1 = 0
        r.content =f'{c1}\n```\n{c2}\n```'
        print(f'>>> {bcolors.OKGREEN}Command: K8s.projects success.{bcolors.ENDC}')
        reply(r)

    def getNodes(self, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content = f'One moment; running `!k8s nodes`'
        reply(r)
        r.content = 'These are all of the nodes in the current active cluster:\n\n'
        print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
        for node in self.__intKubernetes.getNodes():
            r.content+=f'    {node["name"]}: {node["conditions"]}\n'
        print(f'>>> {bcolors.OKGREEN}Command: K8s.nodes success.{bcolors.ENDC}')
        reply(r)

    def getPods(self,namespace ,args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
        if len(args) != 0:
            pod_name = args[0]

            if str(pod_name).lower() in ['running', 'failed', 'unknown', 'succeeded']:
                r.content = f'One moment; running `!k8s {namespace} pods with status {pod_name}`'
                reply(r)

                if len(self.__intKubernetes.getPods(namespace, args)) == 0:
                    r.content = f'No pods found for status {pod_name}'

                else:
                    r.content = 'These are all of the pods in the requested namespace:\n\n'
                    r.content+= '| Pod Name | Status |\n'
                    r.content+= '|:---------- |:----:| ----:|\n'
                    for node in self.__intKubernetes.getPods(namespace, args):
                        r.content+=f'|{node["name"]},|{node["status"]}|\n'

                print(f'>>> {bcolors.OKGREEN}Command: K8s.nodes success.{bcolors.ENDC}')
            
            else:
                if 'describe' in args:
                    r.content = f'One moment; running `!k8s {namespace} pods {pod_name} description`'
                    reply(r)
                    r.content = 'Showing description in the specific pod of a namespace :\n\n'
                    
                    description = self.__intKubernetes.getPodDescription(namespace, pod_name)
                    print(description)

                    r.content+= '%s' % description
                else:
                    r.content = f'One moment; running `!k8s {namespace} pods {pod_name}`'
                    reply(r)
                    r.content = 'Showing logs in the specific pod of a namespace :\n\n'
                    
                    logs = self.__intKubernetes.getPodLogs(namespace, pod_name)
                    r.content+= '%s' % logs
        else:
            r.content = f'One moment; running `!k8s {namespace} pods`'
            reply(r)
            if len(self.__intKubernetes.getPods(namespace, args)) == 0:
                r.content = f'No pods found for status {pod_name}'
                    
            else:
                r.content = 'These are all of the pods in the requested namespace:\n\n'
                r.content+= '| Pod Name | Status |\n'
                r.content+= '|:---------- |:----:| ----:|\n'
                for node in self.__intKubernetes.getPods(namespace, args):
                    r.content+=f'|{node["name"]},|{node["status"]}|\n'

            print(f'>>> {bcolors.OKGREEN}Command: K8s.nodes success.{bcolors.ENDC}')

        print({namespace})
        reply(r)

    def getDeployments(self, namespace, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content = f'One moment; running `!k8s {namespace} deployments`'
        reply(r)
        noEmpty = True
        c1 =f'Hey @{m.sender}! Namespace `{namespace}` has the following deployments:\n\n```\n'
        c2 = 'These deployments are empty (0/0) :\n\n```\n'
        ctr1 = 0
        ctr2 = 0
        print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
        for project in self.__intKubernetes.getDeployments(namespace):
            if (project['available'] < 1):
                c2+=f'{project["name"]} ({project["available"]}/{project["replicas"]})'.ljust(40)
                ctr2+= 1
                noEmpty = False
            else:
                c1+=f'{project["name"]} ({project["available"]}/{project["replicas"]})'.ljust(40)
                ctr1+= 1
            if (ctr2 == 3):
                c2+= '\n'
                ctr2 = 0
            if (ctr1 == 3):
                c1+= '\n'
                ctr1 = 0
        if (noEmpty):
            r.content =f'{c1}\n```'
        else:
            r.content =f'{c1}\n```\n{c2}\n```'
        print(f'>>> {bcolors.OKGREEN}Command: K8s.namespace.deploy success.{bcolors.ENDC}')
        reply(r)

    def getImage(self, namespace, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        if (args.__len__() < 1):
            r.content =f'Microservice not found in Namespace `{namespace}`'
            print(f'>>> {bcolors.FAIL}Command: K8s.namespace.image failed: Microservice not found in Namespace.{bcolors.ENDC}')
        else:
            print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
            deployments = self.__intKubernetes.getDeploymentsFast(namespace)
            deploymentMaybe = next((s for s in deployments if args[0].lower() in s), None)
            if (deploymentMaybe is None):
                r.content =f'Microservice `{args[0]}` not found in Namespace `{namespace}`'
                print(f'>>> {bcolors.FAIL}Command: K8s.namespace.image failed: Microservice not found in Namespace.{bcolors.ENDC}')
            else:
                r.content =f'One moment; running `!k8s {namespace} image {deploymentMaybe}`'
                reply(r)
                image = self.__intKubernetes.getImage(namespace, deploymentMaybe)
                r.content =f'Microservice `{deploymentMaybe}` in Namespace `{namespace}` uses Image `{image}`'
        print(f'>>> {bcolors.OKGREEN}Command: K8s.namespace.image success.{bcolors.ENDC}')
        reply(r)

    def getEnvVars(self, namespace, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        if (args.__len__() < 1):
            r.content =f'Microservice not found in Namespace `{namespace}`'
            print(f'>>> {bcolors.FAIL}Command: K8s.namespace.env failed: Microservice not found in Namespace.{bcolors.ENDC}')
        else:
            print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
            deployments = self.__intKubernetes.getDeploymentsFast(namespace)
            deploymentMaybe = next((s for s in deployments if args[0].lower() in s), None)
            if (deploymentMaybe is None):
                r.content =f'Microservice `{args[0]}` not found in Namespace `{namespace}`'
                print(f'>>> {bcolors.FAIL}Command: K8s.namespace.env failed: Microservice not found in Namespace.{bcolors.ENDC}')
            else:
                r.content =f'One moment; running `!k8s {namespace} env {deploymentMaybe}`'
                reply(r)
                r.content =f'Microservice `{deploymentMaybe}` in Namespace `{namespace}` has the following ENV Vars:\n\n'
                r.content+= '| key | value |\n'
                r.content+= '|:--- | -----:|\n'
                for envVar in self.__intKubernetes.getEnvVars(namespace, deploymentMaybe):
                    r.content+=f'|{envVar["key"]}|{envVar["value"]}|\n'
        print(f'>>> {bcolors.OKGREEN}Command: K8s.namespace.env success.{bcolors.ENDC}')
        reply(r)

    def getEndpoints(self, namespace, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        if (args.__len__() < 1):
            r.content =f'Microservice not found in Namespace `{namespace}`'
            print(f'>>> {bcolors.FAIL}Command: K8s.namespace.endpoint failed: Microservice not found in Namespace.{bcolors.ENDC}')
        else:
            print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
            deployments = self.__intKubernetes.getDeploymentsFast(namespace)
            deploymentMaybe = next((s for s in deployments if args[0].lower() in s), None)
            if (deploymentMaybe is None):
                r.content =f'Microservice `{args[0]}` not found in Namespace `{namespace}`'
                print(f'>>> {bcolors.FAIL}Command: K8s.namespace.endpoint failed: Microservice not found in Namespace.{bcolors.ENDC}')
            else:
                r.content =f'One moment; running `!k8s {namespace} endpoints {deploymentMaybe}`'
                reply(r)
                r.content =f'Microservice `{deploymentMaybe}` in Namespace `{namespace}` has the following Endpoints:\n\n'
                r.content+= '| IP Address | Name | Port | Protocol |\n'
                r.content+= '|:---------- |:----:| ----:| --------:|\n'
                endpoints = self.__intKubernetes.getEndpoints(namespace, deploymentMaybe)
                if (endpoints.__len__() < 1):
                    r.content =f'Microservice `{deploymentMaybe}` in Namespace `{namespace}` doesn\'t have any Endpoints.'
                else:
                    for endpoint in endpoints:
                        r.content+=f'|{endpoint["address"]}|{endpoint["name"]}|{endpoint["port"]}|{endpoint["protocol"]}|\n'
        print(f'>>> {bcolors.OKGREEN}Command: K8s.namespace.endpoint success.{bcolors.ENDC}')
        reply(r)

    def getLogs(self, namespace, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        if (args.__len__() < 1):
            r.content =f'Microservice not found in Namespace `{namespace}`'
            print(f'>>> {bcolors.FAIL}Command: K8s.namespace.logs failed: Microservice not found in Namespace.{bcolors.ENDC}')
        else:
            print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
            deployments = self.__intKubernetes.getDeploymentsFast(namespace)
            deploymentMaybe = next((s for s in deployments if args[0].lower() in s), None)
            if (deploymentMaybe is None):
                r.content =f'Microservice `{args[0]}` not found in Namespace `{namespace}`'
                print(f'>>> {bcolors.FAIL}Command: K8s.namespace.logs failed: Microservice not found in Namespace.{bcolors.ENDC}')
            else:
                if (args.__len__() < 2):
                    args.append('10')
                try:
                    tail = int(args[1])
                except Exception:
                    r.content = 'Unable to parse tail count, showing 10...'
                    reply(r)
                    tail = 10
                r.content =f'One moment; running `!k8s {namespace} logs {deploymentMaybe} {str(tail)}`'
                reply(r)
                r.content =f'Hey @{m.sender}! Here\'s the logs from Microservice `{deploymentMaybe}`' + \
                           f' in Namespace `{namespace}` (tail {str(tail)}):\n\n``` text\n'
                for log in self.__intKubernetes.getLogs(namespace, deploymentMaybe, tail):
                    r.content+=f'{log}'
                r.content+= '\n```'
        print(f'>>> {bcolors.OKGREEN}Command: K8s.namespace.logs success.{bcolors.ENDC}')
        reply(r)

    def getRGStatus(self, resourceGroup, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'One moment; running `!k8s {resourceGroup} status`'
        reply(r)
        r.content =f'Hey @{m.sender}! Here\'s the current status of `{resourceGroup}`:\n\n'
        r.content+=f'| Resource | State |\n'
        r.content+=f'|:-------- | -----:|\n'
        # for state in self.__intMSAzure.getResourceGroupStatus(resourceGroup):
        #    r.content+=f'|{state["name"]}|{state["state"]}|\n'
        print(f'>>> {bcolors.OKGREEN}Command: K8s.resourceGroup.status success.{bcolors.ENDC}')
        reply(r)

    def scaleDeployments(self, namespace, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        if (args.__len__() < 1):
            r.content =f'Microservice not found in Namespace `{namespace}`'
            print(f'>>> {bcolors.FAIL}Command: K8s.namespace.scale failed: Microservice not found in Namespace.{bcolors.ENDC}')
        else:
            print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
            deployments = self.__intKubernetes.getDeploymentsFast(namespace)
            deploymentMaybe = next((s for s in deployments if args[0].lower() in s), None)
            if (deploymentMaybe is None):
                r.content =f'Microservice `{args[0]}` not found in Namespace `{namespace}`'
                print(f'>>> {bcolors.FAIL}Command: K8s.namespace.logs failed: Microservice not found in Namespace.{bcolors.ENDC}')
            else:
                if (args.__len__() < 2):
                    args.append('1')
                try:
                    replicas = int(args[1])
                except Exception:
                    r.content = 'Unable to parse replicas, scaling to 1...'
                    reply(r)
                    replicas = 1
                r.content =f'One moment; running `!k8s {namespace} scale {deploymentMaybe} {str(replicas)}`'
                reply(r)
                self.__intKubernetes.scaleDeployment(namespace, deploymentMaybe, replicas)
                r.content =f'Hey @{m.sender}! `{deploymentMaybe}` of `{namespace}` successfully scaled. ' + \
                           f'Current replicas: {str(replicas)}.'
        print(f'>>> {bcolors.OKGREEN}Command: K8s.namespace.scale success.{bcolors.ENDC}')
        reply(r)

    def scaleAll(self, namespace, args, m, reply):
        r = Message('CFGUILDBOT/k8s')
        r.channelId = m.channelId
        r.channel = m.channel
        print(f'>>> {bcolors.OKBLUE}Processing via Kubernetes Integration..{bcolors.ENDC}')
        deployments = self.__intKubernetes.getDeploymentsFast(namespace)

        try:
            replicas = int(args[0])
        except Exception:
            r.content = 'Unable to parse replicas, scaling to 1...'
            reply(r)
            replicas = 1

        r.content =f'One moment; running `!k8s {namespace} scale-all {str(replicas)}`'
        reply(r)

        for deployment in deployments:
            self.__intKubernetes.scaleDeployment(namespace, deployment, replicas)
            r.content =f'`{deployment}` of `{namespace}` successfully scaled. ' + \
                f'Current replicas: {str(replicas)}.'
            reply(r)

        print(f'>>> {bcolors.OKGREEN}Command: K8s.namespace.scale success.{bcolors.ENDC}')
