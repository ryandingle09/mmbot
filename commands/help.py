from channels.message import Message
import shlex, bcolors
from logger import Logger

class Help():
    def __init__(self, botPrefix: str):
        self.__botPrefix = botPrefix
        self.__cmdPrefix = f'{botPrefix}help'

    def reciever(self, m: Message, reply):

        if (m.content is not None):
            try:
                content = shlex.split(m.content)
            except:
                return
            if (m.content.lower().startswith(self.__cmdPrefix)):
                try:
                    content.remove(self.__cmdPrefix)
                except:
                    return
                if (content.__len__() == 0):
                    content.append('?')
                subcommand = content.pop(0)
                print(f'>>> {bcolors.OKBLUE}Command: Help.{subcommand} recieved from {m.source}/{m.channel}/{m.sender}{bcolors.ENDC}')
                print(m.roles)
                print(m.groups)

                Logger.send_logs(m.content, m.sender)

                # Switch to subcommands HERE
                if (subcommand.lower() in ['?', 'h', 'help']):
                    self.helpHeader(m, reply)
                    self.helpHelp(m, reply)
                    print(f'>>> {bcolors.OKGREEN}Command: Help.help success.{bcolors.ENDC}')
                elif (subcommand.lower() in ['all']):
                    self.helpHeader(m, reply, True)

                    if 'exec' in m.groups or 'dice' in m.groups:
                        self.helpGeneral(m, reply, True)

                    #self.helpRole(m, reply, True)
                    
                    if 'alerts' in m.groups:
                        self.helpAlerts(m, reply, True)
                    
                    if 'general kubernetes' in m.groups:
                        self.helpK8s(m, reply, True)
                    
                    if 'deployments' in m.groups:
                        self.helpK8sDeploy(m, reply, True)
                    
                    if 'image' in m.groups:
                        self.helpK8sImage(m, reply, True)
                    
                    if 'pods' in m.groups:
                        self.helpK8sPod(m, reply, True)
                    
                    if 'azure' in m.groups:
                        self.helpK8sAzure(m, reply, True)

                    if 'production-transaction' in m.groups:
                        self.helpProductionTransaction(m, reply, True)
                    
                    if 'production-onboarding' in m.groups:
                        self.helpProductionOnboarding(m, reply, True)

                    r = Message('CFGUILDBOT/help')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content =f'`!help all` Direct Message sent to @{m.sender}.'
                    reply(r)
                elif (subcommand.lower() in ['general', 'g']):
                    self.helpHeader(m, reply)
                    self.helpGeneral(m, reply)
                # elif (subcommand.lower() in ['role', 'r']):
                #     self.helpHeader(m, reply)
                #     self.helpRole(m, reply)
                elif (subcommand.lower() in ['alerts', 'a']):
                    if 'alerts' not in m.groups:
                        self.group_checker('alerts', m, reply)
                    else:
                        self.helpHeader(m, reply)
                        self.helpAlerts(m, reply)
                elif (subcommand.lower() in ['k8s', 'k']):
                    if 'general kubernetes' not in m.groups:
                        self.group_checker('k8s', m, reply)
                    else:
                        self.helpHeader(m, reply)
                        self.helpK8s(m, reply)
                elif (subcommand.lower() in ['deploy', 'd']):
                    if 'deploy' not in m.groups:
                        self.group_checker('deploy', m, reply)
                    else:
                        self.helpHeader(m, reply)
                        self.helpK8sDeploy(m, reply)
                elif (subcommand.lower() in ['image', 'i']):
                    if 'image' not in m.groups:
                        self.group_checker('image', m, reply)
                    else:
                        self.helpHeader(m, reply)
                        self.helpK8sImage(m, reply)
                elif (subcommand.lower() in ['pod', 'p']):
                    if 'pods' not in m.groups:
                        self.group_checker('pods', m, reply)
                    else:
                        self.helpHeader(m, reply)
                        self.helpK8sPod(m, reply)
                elif (subcommand.lower() in ['azure', 'az']):
                    if 'azure' not in m.groups:
                        self.group_checker('azure', m, reply)
                    else:
                        self.helpHeader(m, reply)
                        self.helpK8sAzure(m, reply)
                elif (subcommand.lower() in ['elk-o', 'eo']):
                    if 'production-onboarding' not in m.groups:
                        self.group_checker('production-onboarding', m, reply)

                    else:
                        self.helpHeader(m, reply)
                        self.helpProductionOnboarding(m, reply)

                elif (subcommand.lower() in ['elk-t', 'et']):
                    if 'production-transaction' not in m.groups:
                        self.group_checker('production-transaction', m, reply)

                    else:
                        self.helpHeader(m, reply)
                        self.helpProductionTransaction(m, reply)

                else:
                    r = Message('CFGUILDBOT/help')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content = 'Command not found :('
                    print(f'>>> {bcolors.FAIL}Command: Help.{subcommand} not found.{bcolors.ENDC}')
                    reply(r)

    def role_checker(self, role, m, reply, DM: bool = False):
        if str(role) not in m.roles:
            r = Message('CFGUILDBOT/help')
            r.channelId = m.channelId
            r.channel = m.channel
            r.content = ':warning: You do not have permission to perform this command.'
            reply(r, m.senderId if DM else None)
    
    def group_checker(self, group, m, reply, DM: bool = False):
        if str(group) not in m.groups:
            r = Message('CFGUILDBOT/help')
            r.channelId = m.channelId
            r.channel = m.channel
            r.content = ':warning: You do not have permission to perform this command.'
            reply(r, m.senderId if DM else None)

    def helpHeader(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content = '# :imp: Vader :imp: The DevOps (CF Guild) Bot.\n'
        r.content+= 'For more information/access please contact the DevOps team. Type ' + \
                    '`!help` to show all bot categories.\nLegend: (options) are **required**, ' + \
                    '<options> are **optional** and [options] will **match partial** words.\n\n'
        r.content+= '---\n'
        reply(r, m.senderId if DM else None)

    def helpHelp(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'`{"!help all ".ljust(40, "·")}` :white_check_mark: All Commands (too long, will be sent via DM)\n'

        if 'roll' in m.roles and 'exec' in m.roles:
            r.content+=f'`{"!help general|g ".ljust(40, "·")}` :white_check_mark: General & Uncategorized Commands\n'

        #r.content+=f'`{"!help role|r ".ljust(40, "·")}` :white_check_mark: Role Management/Access Control Commands\n''

        if 'alerts' in m.groups:
            r.content+=f'`{"!help alerts|a ".ljust(40, "·")}` :white_check_mark: Grafana/Kibana Alerts & Subscription Commands\n'
        
        if 'general kubernetes' in m.groups:
            r.content+=f'`{"!help k8s|k ".ljust(40, "·")}` :white_check_mark: General Kubernetes/OpenShift Commands\n'
        
        if 'deploy' in m.groups:
            r.content+=f'`{"!help deploy|d ".ljust(40, "·")}` :white_check_mark: Kubernetes/OpenShift Commands regarding Deployments\n'

        if 'image' in m.groups:
            r.content+=f'`{"!help image|i ".ljust(40, "·")}` :white_check_mark: Kubernetes/OpenShift Commands regarding Images\n'
        
        if 'pods' in m.groups:
            r.content+=f'`{"!help pod|p ".ljust(40, "·")}` :white_check_mark: Kubernetes/OpenShift Commands regarding Pods\n'

        #r.content+=f'`{"!help svc|s ".ljust(40, "·")}` :building_construction: Kubernetes/OpenShift Commands regarding Services\n'

        if 'azure' in m.groups:
            r.content+=f'`{"!help azure|az ".ljust(40, "·")}` :white_check_mark: General Azure Commands\n'

        if 'terraform' in m.groups:
            r.content+=f'`{"!help terraform|tf ".ljust(40, "·")}` :building_construction: General Terraform Commands\n'

        if 'production-onboarding' in m.groups:
            r.content+=f'`{"!help elk-o|eo ".ljust(40, "·")}` :building_construction: Production related commands for onboarding checking\n'

        if 'production-transaction' in m.groups:
            r.content+=f'`{"!help elk-t|et ".ljust(40, "·")}` :building_construction:  Production related commands for transaction checking\n'

        reply(r, m.senderId if DM else None)

    def helpGeneral(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## General & Uncategorized Commands\n'

        if 'roll' in m.roles:
            r.content+=f'`{"!roll <dice> ".ljust(40,"·")}` :white_check_mark: Roll some dice. Default: 1d6 (one dice with six sides)\n'
        if 'exec' in m.roles:
            r.content+=f'`{"!exec (command) ".ljust(40,"·")}` :white_check_mark: Execute a bash command at the chatops server.\n'

        reply(r, m.senderId if DM else None)

    def helpRole(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## Role Management/Access Control Commands\n'
        r.content+=f'`{"!role ".ljust(40,"·")}` :building_construction: Check your current role and it\'s access to bot commands.\n'
        r.content+=f'`{"!role request|r (newRole)".ljust(40,"·")}` :building_construction: Request to switch to (newRole).\n'
        r.content+=f'`{"!role request|r list|ls".ljust(40,"·")}` :building_construction: List all requests for role changes.\n'
        r.content+=f'`{"!role request|r accept|a (requestNo)".ljust(40,"·")}` :building_construction: Accept a request for role change.\n'
        r.content+=f'`{"!role request|r decline|d (requestNo)".ljust(40,"·")}` :building_construction: Decline a request for role change.\n'
        r.content+=f'`{"!role access|a list|ls (role)".ljust(40,"·")}` :building_construction: Check (role)\'s access to bot commands.\n'
        r.content+=f'`{"!role access|a add|a (role) (command)".ljust(40,"·")}` :building_construction: Add (command) access to (role).\n'
        r.content+=f'`{"!role access|a rm|r (role) (command)".ljust(40,"·")}` :building_construction: Remove (command) access to (role).\n'
        reply(r, m.senderId if DM else None)

    def helpAlerts(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## Grafana/Kibana Alerts & Subscription Commands\n'
        r.content+=f'`{"!alerts ".ljust(40,"·")}` :white_check_mark: List all alerts in Grafana and Kibana\n'

        if 'list' in m.roles:
            r.content+=f'`{"!alerts list|ls <filter>".ljust(40,"·")}` :building_construction: List alerts according to <filter>. ' + \
                    f'Default: All. Can also be AlertsOnly or StableOnly\n'

        if 'subscribe' in m.roles:
            r.content+=f'`{"!alerts subscribe|s (source) (alert)".ljust(40,"·")}` :building_construction: The bot will begin ' + \
                    f'DM\'ing you changes to (source)\'s (alert).\n'

        if 'unsubscribe' in m.roles:
            r.content+=f'`{"!alerts unsubscribe|u (source) (alert)".ljust(40,"·")}` :building_construction: The bot will stop' + \
                    f' DM\'ing you changes to (source)\'s (alert).\n'

        reply(r, m.senderId if DM else None)

    def helpK8s(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## General Kubernetes/OpenShift Commands\n'
        r.content+=f'`{"!k8s ".ljust(40,"·")}` :white_check_mark: Lists all projects/namespaces in the current cluster.\n'

        if 'cluster' in m.roles:
            r.content+=f'`{"!k8s cluster|c ".ljust(40,"·")}` :white_check_mark: Shows the current active cluster.\n'

        if 'nodes' in m.roles:
            r.content+=f'`{"!k8s nodes|n ".ljust(40,"·")}` :white_check_mark: Lists all nodes in the current cluster.\n'
            r.content+=f'`{"!k8s nodes|n ".ljust(40,"·")}` ::building_construction:  Drain Node\n'
        
        if 'scale-all' in m.roles:
            r.content+=f'`{"REFER TO k8sDeploy scale-all".ljust(40,"·")}` ::white_check_mark: Entire Project Scaling\n'

        reply(r, m.senderId if DM else None)

    def helpK8sDeploy(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## Kubernetes/OpenShift Commands regarding Deployments\nNote: first partial ' + \
                   f'match applies, eg. `cli` == `clientservices` & `boo` == `addressbook`\n'
        r.content+=f'`{"!k8s [project/namespace]".ljust(40,"·")}` :white_check_mark: Lists all deployments on the [project].\n'

        if 'env' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] env [deployment]".ljust(40,"·")}` :white_check_mark: Lists the [deployment]\'s Environment Variables.\n'

        if 'envCompare' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] envCompare|envc [dply] [prj2]".ljust(40,"·")}` :building_construction: Compares the [deployment]\'s ' + \
                    f'Env Variables between [prj] and [prj2].\n'

        if 'end' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] end [deployment]".ljust(40,"·")}` :white_check_mark: Lists the [deployment]\'s Endpoints.\n'
        
        if 'endCompare' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] endCompare|endc [dply] [prj2]".ljust(40,"·")}` :building_construction: Compares the [deployment]\'s ' + \
                    f'Endpoints between [project/namespace] and [project/namespace].\n'
        
        if 'logs' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] logs [deployment] <tail>".ljust(40,"·")}` :white_check_mark: Takes the <tail> of the logs on the ' + \
                    f'[deployment]. Default: 10.\n'

        if 'scale' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] scale [deployment] <replica>".ljust(40,"·")}` :white_check_mark: Scales the deployment to <replica>. ' + \
                    f'Default: 1.\n'
        
        if 'scale-all' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] scale-all <replica>".ljust(40,"·")}` :white_check_mark: Scales all deployments to <replica>. ' + \
                    f'Default: 1.\n'

        r.content+=f'`{"TODO".ljust(40,"·")}` :building_construction: Redeploy (scale down then up again)\n'
        r.content+=f'`{"TODO".ljust(40,"·")}` :building_construction: Redeploy Pending (scale down then up again)\n'
        r.content+=f'`{"TODO".ljust(40,"·")}` :building_construction: Describe Deployment (DM the YAML)\n'
        reply(r, m.senderId if DM else None)

    def helpK8sImage(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## Kubernetes/OpenShift Commands regarding Images\nNote: first partial ' + \
                   f'match applies, eg. `cli` == `clientservices` & `boo` == `addressbook`\n'

        if 'image' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] image|i [deployment]".ljust(40,"·")}` :white_check_mark: Shows the current applied image for the [deployment].\n'

        if 'imageDeploy' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] imageDeploy|id [dply] (hash)".ljust(40,"·")}` :building_construction: Edits the image hash of the ' + \
                    f'[deployment] and applies it.\n'

        if 'imageCompare' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] imageCompare|ic [dply] [project/namespace]".ljust(40,"·")}` :building_construction: Compares the [deployment]\'s images ' + \
                    f'between [project/namespace] and [prj2].\n'
        
        if 'imageCompareall' in m.roles:
            r.content+=f'`{"!k8s  [project/namespace] imageCompareAll|ica [project/namespace]".ljust(40,"·")}` :building_construction: Compares all images between [prj] and [prj2].\n'
        
        r.content+=f'`{"TODO".ljust(40,"·")}` :building_construction: Describe Image (DM the YAML)\n'
        reply(r, m.senderId if DM else None)

    def helpK8sPod(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## Kubernetes/OpenShift Commands regarding Pods\nNote: first partial ' + \
                   f'match applies, eg. `cli` == `clientservices` & `boo` == `addressbook`\n'

        if 'pods' in m.roles:
            r.content+=f'`{"!k8s [project/namespace] pods".ljust(40,"·")}` :white_check_mark: List Pods on Project, with status\n'
            r.content+=f'`{"!k8s [project/namespace] pods [podname]".ljust(40,"·")}` :white_check_mark: Takes the latest 90 lines of logs on the ' + \
                    f'[specific pod].\n'
            r.content+=f'`{"!k8s [project/namespace] pods [running|failed|unknown|succeeded]".ljust(40,"·")}` :white_check_mark: List Pods on Project, with status (Filtered)\n'
            # r.content+=f'`{"!k8s [project/namespace] pods [podname] describe".ljust(40,"·")}` :white_check_mark: Describe Pod\n'
        
        #r.content+=f'`{"TODO".ljust(40,"·")}` :building_construction: Subscribe to Project\'s Pod Status Changes\n'
        #r.content+=f'`{"TODO".ljust(40,"·")}` :building_construction: Unsubscribe to Project\'s Pod Status Changes\n'
        reply(r, m.senderId if DM else None)

    def helpK8sAzure(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## General Azure Commands\nNote: first partial match applies, eg. `devs` == `ingasiadevstg`\n'
        r.content+=f'`{"!k8s [resourceGroup]".ljust(40,"·")}` :no_entry: Check the status of VMs in the [resourceGroup].\n'
        
        if 'billing' in m.roles:
            r.content+=f'`{"!k8s [resourceGroup] billing|b".ljust(40,"·")}` :no_entry: Check billing statement of [resourceGroup].\n'
                
        reply(r, m.senderId if DM else None)

    def helpProductionOnboarding(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## Production Checking Commands for Onboarding\n'

        if 'onboarding-success' in m.roles:
            r.content+=f'`{"!elk onboarding-success".ljust(40,"·")}` :building_construction: Show Total Count of Successfully Onboarded Customers Today.\n'

        if 'onboarding-failed' in m.roles:
            r.content+=f'`{"!elk onboarding-failed".ljust(40,"·")}` :building_construction: Show Total Count of Failed to Onboard Customers Today.\n'

        if 'kyc-namescreening-status' in m.roles:
            r.content+=f'`{"!elk kyc-namescreening-status".ljust(40,"·")}` :building_construction: Show Status of Onboarding KYC Namescreening.\n'

        if 'kyc-risk-status' in m.roles:
            r.content+=f'`{"!elk kyc-risk-status".ljust(40,"·")}` :building_construction: Show Status of Onboarding KYC Risk Namescreening.\n'

        if 'otp-status' in m.roles:
            r.content+=f'`{"!elk otp-status".ljust(40,"·")}` :building_construction: Show Status of Onboarding OTP Today.\n'

        if 'otp-aws-status' in m.roles:
            r.content+=f'`{"!elk otp-aws-status".ljust(40,"·")}` :building_construction: Show Status of Onboarding third party AWS.\n'

        if 'cif-creation-status' in m.roles:
            r.content+=f'`{"!elk cif-creation-status".ljust(40,"·")}` :building_construction: Show Status of Onboarding CIF Creation.\n'

        if 'face-liveness-status' in m.roles:
            r.content+=f'`{"!elk face-liveness-status".ljust(40,"·")}` :building_construction: Show Status of Onboarding Face++.\n'

        if 'id-upload-status' in m.roles:
            r.content+=f'`{"!elk id-upload-status".ljust(40,"·")}` :building_construction: Show Status of Onboarding ID Upload.\n'

        if 'account-summary-status' in m.roles:
            r.content+=f'`{"!elk account-summary-status".ljust(40,"·")}` :building_construction: Show Status of Onboarding Account Summary.\n'

        if 'onboarding-batch-status' in m.roles:
            r.content+=f'`{"!elk onboarding-batch-status".ljust(40,"·")}` :building_construction: Show Status of Onboarding Batch.\n'
                
        reply(r, m.senderId if DM else None)

    def helpProductionTransaction(self, m, reply, DM: bool = False):
        r = Message('CFGUILDBOT/help')
        r.channelId = m.channelId
        r.channel = m.channel
        r.content =f'## Production Checking Commands for Transaction\n'

        if 'pesonet-actf' in m.roles:
            r.content+=f'`{"!elk pesonet-actf".ljust(40,"·")}` :building_construction: Show Pesonet After Cutoff Transfer Failed Count Today.\n'
        
        if 'credit-tsf' in m.roles:
            r.content+=f'`{"!elk credit-tsf".ljust(40,"·")}` :building_construction: Show Credit to Suspense Failed Count Today.\n'
        
        if 'check-dfc' in m.roles:
            r.content+=f'`{"!elk check-dfc".ljust(40,"·")}` :building_construction: Show Check Deposit Failed Count Today.\n'
        
        if 'failed-cr' in m.roles:
            r.content+=f'`{"!elk failed-cr".ljust(40,"·")}` :building_construction: Show Check Deposit Return Failed Count Today.\n'
        
        if 'realease-hcf' in m.roles:
            r.content+=f'`{"!elk realease-hcf".ljust(40,"·")}` :building_construction: Show Release Hold Checks Failed Count Today.\n'
        
        if 'transaction-attempts' in m.roles:
            r.content+=f'`{"!elk transaction-attempts".ljust(40,"·")}` :building_construction: Show Transaction Attempts Count Today.\n'
        
        if 'success-it' in m.roles:
            r.content+=f'`{"!elk success-it".ljust(40,"·")}` :building_construction: Show Successfult Internal Transfer Count.\n'
        
        if 'internal-tf' in m.roles:
            r.content+=f'`{"!elk internal-tf".ljust(40,"·")}` :building_construction: Show Internal Transfer Failed Count Today.\n'
        
        if 'intraday-et' in m.roles:
            r.content+=f'`{"!elk intraday-et".ljust(40,"·")}` :building_construction: Show Intraday External Transfer Count Today.\n'
        
        if 'after-cpets' in m.roles:
            r.content+=f'`{"!elk after-cpets".ljust(40,"·")}` :building_construction: Show After Cutoff Pesonet External Transfer Success Count Today.\n'
        
        if 'external-tlf' in m.roles:
            r.content+=f'`{"!elk external-tlf".ljust(40,"·")}` :building_construction: Show External Transfer Limit Failed Count Today.\n'
        
        if 'debit-ss' in m.roles:
            r.content+=f'`{"!elk debit-ss".ljust(40,"·")}` :building_construction: Show Debit from Suspense Success Count Today.\n'
        
        if 'debit-sf' in m.roles:
            r.content+=f'`{"!elk debit-sf".ljust(40,"·")}` :building_construction: Show Debit from Suspense Failed Count Today.\n'
        
        if 'pesonet-ics' in m.roles:
            r.content+=f'`{"!elk pesonet-ics".ljust(40,"·")}` :building_construction: Show Pesonet Inward Credit Success Count Today.\n'
        
        if 'pesonet-icf' in m.roles:
            r.content+=f'`{"!elk pesonet-icf".ljust(40,"·")}` :building_construction: Show Pesonet Inward Credit Failed Count Today.\n'
        
        if 'pesonet-css' in m.roles:
            r.content+=f'`{"!elk pesonet-css".ljust(40,"·")}` :building_construction: Show Pesonet Credit to Suspense Success Count Today.\n'
        
        if 'settled-if' in m.roles:
            r.content+=f'`{"!elk settled-if".ljust(40,"·")}` :building_construction: Show Pesonet Inward Reversal Count Today.\n'
        
        if 'settled-if' in m.roles:
            r.content+=f'`{"!elk settled-if".ljust(40,"·")}` :building_construction: Show Settled Inward Transactions Count Todayy.\n'
        
        if 'check-ds' in m.roles:
            r.content+=f'`{"!elk check-ds".ljust(40,"·")}` :building_construction: Show Check Deposit Success Count Today.\n'
        
        if 'check-dar' in m.roles:
            r.content+=f'`{"!elk check-dar".ljust(40,"·")}` :building_construction: Show Check Deposit Attempt Reversal Count Today.\n'
        
        if 'check-dfch' in m.roles:
            r.content+=f'`{"!elk check-dfch".ljust(40,"·")}` :building_construction: Show Check Deposit Failed Check Hold Count Today.\n'
        
        if 'check-fir' in m.roles:
            r.content+=f'`{"!elk check-fir".ljust(40,"·")}` :building_construction: Show Front Image Rescans Count Today.\n'
        
        if 'check-rir' in m.roles:
            r.content+=f'`{"!elk check-rir".ljust(40,"·")}` :building_construction: Show Rear Image Rescans Count Today.\n'
        
        if 'transaction-ci' in m.roles:
            r.content+=f'`{"!elk transaction-ci".ljust(40,"·")}` :building_construction: Show Connection Issues Count Today.\n'
        
        if 'transaction-se' in m.roles:
            r.content+=f'`{"!elk transaction-se".ljust(40,"·")}` :building_construction: Show System Error Count Today.\n'
        
        if 'rejected-cd' in m.roles:
            r.content+=f'`{"!elk rejected-cd".ljust(40,"·")}` :building_construction: Show Rejected Check Deposit Count Today.\n'
        
        if 'instapay-stm' in m.roles:
            r.content+=f'`{"!elk instapay-stm".ljust(40,"·")}` :building_construction: Show Instapay Success Transactions Metric Count Today.\n'
        
        if 'check-drs' in m.roles:
            r.content+=f'`{"!elk check-drs".ljust(40,"·")}` :building_construction: Show Check Deposit Return Success Count Today.\n'
        
        if 'instapay-ftm' in m.roles:
            r.content+=f'`{"!elk instapay-ftm".ljust(40,"·")}` :building_construction: Show Instapay Failed Transaction Metric Count Today.\n'
        
        if 'release-hcs' in m.roles:
            r.content+=f'`{"!elk release-hcs".ljust(40,"·")}` :building_construction: Show Release Hold Checks Success Count Today.\n'
        
        if 'instapay-fvtm' in m.roles:
            r.content+=f'`{"!elk instapay-fvtm".ljust(40,"·")}` :building_construction: Show Instapay For Validation Transaction Metric Count Today.\n'
                
        reply(r, m.senderId if DM else None)

