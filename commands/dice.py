from channels.message import Message
from functools import reduce
import shlex, random, operator, bcolors
from logger import Logger

class Dice:
    def __init__(self, botPrefix: str):
        self.__botPrefix = botPrefix
        self.__cmdPrefix = f'{botPrefix}roll'

    def reciever(self, m: Message, reply):
        if (m.content is not None):
            try:
                content = shlex.split(m.content)
            except:
                return
            if (m.content.lower().startswith(self.__cmdPrefix)):
                try:
                    content.remove(self.__cmdPrefix)
                except:
                    return
                    
                Logger.send_logs(m.content, m.sender)

                if 'Dice' not in m.group or 'roll' not in m.roles:
                    r = Message('CFGUILDBOT/test dice roll')
                    r.channelId = m.channelId
                    r.channel = m.channel
                    r.content = ':warning: You do not have permission to perform this command.'
                    reply(r)
                    
                    return

                print(f'>>> {bcolors.OKBLUE}Command: Dice.roll recieved from {m.source}/{m.channel}/{m.sender}{bcolors.ENDC}')
                r = Message('CFGUILDBOT/test dice roll')
                r.content = '' 
                try:
                    result = self.rollDice(" ".join(content))
                    print(f'>>> {bcolors.OKGREEN}Command: Dice.roll success.{bcolors.ENDC}')
                except Exception:
                    r.content = 'Unable to parse dice notation. Here\'s a 1d6 instead:\n'
                    result = self.rollDice('1d6')
                    print(f'>>> {bcolors.FAIL}Command: Dice.roll failed: unable to parse dice notation.{bcolors.ENDC}')
                r.content += f'You rolled a {result}!'
                r.channelId = m.channelId
                r.channel = m.channel
                reply(r)

    def rollDice(self, s: str):
        if ('-' in s):
            return reduce(operator.__sub__, (self.__term(x) for x in s.split('-')))
        else:
            return reduce(operator.__add__, (self.__term(x) for x in s.split('+')))

    def __term(self, t):
        p = t.split('*')
        return self.__factor(p[0]) if len(p)==1 else self.__factor(p[0])*self.__factor(p[1])

    def __factor(self, f):
        g = f.split('d')
        if len(g)==1:
            return int(f)
        return sum(random.randint(1, int(g[1]) if g[1] else 6) for \
                i in range(int(g[0]) if g[0] else 1))
