import requests
import json
from endpoints import role_endpoint

class Role:

    def __init__(self, user_name):
        self.user_name = user_name

    def check_my_role(self):
        uri = "%s/%s" % (role_endpoint, self.user_name)
        r = requests.get(uri)
        data = json.loads(r.text)
        roles = []
        groups = []
        groupings = {}

        if '200' in str(data['status_code']):
            for i in data['user_role']:
                roles.append(i)

            for g in data['user_group']:
                groups.append(g)

            groupings = data['grouping']

        return roles, groups, groupings